use image::{imageops, Rgba, RgbaImage};
use itertools::iproduct;
use qrcodegen::{QrCode, QrCodeEcc};
use superellipse::Superellipse;

#[derive(Debug, Copy, Clone)]
pub enum Shape {
    Circle,
    RoundRect(f64),
    Square,
    Squircle,
}

impl Shape {
    fn get_y(&self, x: f64, quad_size: u32) -> f64 {
        let quad_size = quad_size as f64;

        match self {
            Self::Circle => (quad_size.powi(2) - x.powi(2)).sqrt(),
            Self::RoundRect(curve_percent) => {
                let curve_position = quad_size * (1. - curve_percent);
                if x < curve_position {
                    quad_size
                } else {
                    curve_position
                        + ((quad_size - curve_position).powi(2) - (x - curve_position).powi(2))
                            .sqrt()
                }
            }
            Self::Square => quad_size,
            Self::Squircle => Superellipse::new(4., quad_size, quad_size).get_y(x),
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct PrettyQr {
    anchor_shape: Option<Shape>,
    connect_corners: bool,
    shape: Shape,
    size: u32,
}

macro_rules! square_iproduct {
    ($e:expr) => {
        iproduct!($e, $e)
    };
}

impl PrettyQr {
    pub fn new(size: u32) -> Self {
        Self {
            anchor_shape: None,
            connect_corners: false,
            shape: Shape::Square,
            size,
        }
    }

    pub fn anchor_shape(&mut self, anchor_shape: Shape) -> &mut Self {
        self.anchor_shape = Some(anchor_shape);
        self
    }

    pub fn clear_anchor_shape(&mut self) -> &mut Self {
        self.anchor_shape = None;
        self
    }

    pub fn shape(&mut self, shape: Shape) -> &mut Self {
        self.shape = shape;
        self
    }

    pub fn connect_corners(&mut self, connect_corners: bool) -> &mut Self {
        self.connect_corners = connect_corners;
        self
    }

    pub fn render(&self, data: &[u8]) -> RgbaImage {
        let qr = QrCode::encode_binary(data, QrCodeEcc::Low).unwrap();
        let qr_size = qr.size() as u32;

        let module_size = ((self.size as f64 / qr_size as f64 / 10.).ceil() * 10.) as u32;
        dbg!(qr_size);
        dbg!(module_size);
        let img_size = qr_size * module_size;
        let quad_size = module_size / 2;

        let mut img = RgbaImage::new(img_size, img_size);

        if self.anchor_shape.is_some() {
            self.draw_anchors(&mut img, qr_size, quad_size);
        }

        for (x_module, y_module) in square_iproduct!(0..qr_size) {
            if self.anchor_shape.is_some() {
                let anchor = 2..5;

                let left = anchor.contains(&x_module);
                let top = anchor.contains(&y_module);
                let right = anchor.contains(&(qr_size - 1 - x_module));
                let bottom = anchor.contains(&(qr_size - 1 - y_module));

                if (left && top) || (left && bottom) || (right && top) {
                    continue;
                }
            }

            let is_on = qr.get_module(x_module as i32, y_module as i32);

            let base_x = (module_size * x_module + quad_size) as i32;
            let base_y = (module_size * y_module + quad_size) as i32;

            for (quad_x, quad_y) in square_iproduct!(&[-1, 1]) {
                let base_x = base_x + 0.min(*quad_x);
                let base_y = base_y + 0.min(*quad_y);

                let context = Context {
                    base: (base_x, base_y),
                    is_on,
                    module: (x_module as i32, y_module as i32),
                    qr: &qr,
                    quad: (*quad_x, *quad_y),
                    quad_size,
                };

                let draw = |img: &mut RgbaImage, x: u32, y: u32, alpha: u8| {
                    let color = self.get_color(alpha);

                    // TODO: remove
                    let mut checked_put = |x, y, color| {
                        if x < img.width() && y < img.height() {
                            img.put_pixel(x, y, color);
                        }
                    };

                    checked_put(
                        (base_x + (*quad_x * x as i32)) as u32,
                        (base_y + (*quad_y * y as i32)) as u32,
                        color,
                    );
                    checked_put(
                        (base_x + (*quad_x * y as i32)) as u32,
                        (base_y + (*quad_y * x as i32)) as u32,
                        color,
                    );
                };

                let is_corner = is_corner(&context, self.connect_corners);

                if is_corner {
                    draw_corner(&draw, &mut img, quad_size, is_on, self.shape);
                } else {
                    self.draw_solid_quad(&context, &mut img);
                }
            }
        }

        if img_size == self.size {
            img
        } else {
            imageops::thumbnail(&img, self.size, self.size)
        }
    }

    fn get_color(&self, alpha: u8) -> Rgba<u8> {
        Rgba([0, 0, 0, alpha])
    }

    fn draw_solid_quad(&self, context: &Context, img: &mut RgbaImage) {
        let (base_x, base_y) = context.base;
        let (quad_x, quad_y) = context.quad;

        let color = self.get_color(if context.is_on { 255 } else { 0 });

        for (x, y) in square_iproduct!(0..context.quad_size as i32) {
            img.put_pixel(
                (base_x + (quad_x * x)) as u32,
                (base_y + (quad_y * y)) as u32,
                color,
            );
        }
    }

    fn draw_anchors(&self, mut img: &mut RgbaImage, modules: u32, quad_size: u32) {
        let shape = self.anchor_shape.unwrap();

        let far_offset = quad_size * 2 * (modules - 7);

        let draw = |img: &mut RgbaImage, x: u32, y: u32, alpha: u8| {
            let color = self.get_color(alpha);

            // TODO: remove
            let mut checked_put = |x, y, color| {
                let x = x as u32;
                let y = y as u32;

                if x < img.width() && y < img.height() {
                    img.put_pixel(x, y, color);
                }
            };

            let x = x as i32;
            let y = y as i32;
            for (x, y, (x_offset, y_offset)) in iproduct!(
                &[x, -1 - x],
                &[y, -1 - y],
                &[(0, 0), (far_offset, 0), (0, far_offset)]
            ) {
                let center_x = (x_offset + 7 * quad_size) as i32;
                let center_y = (y_offset + 7 * quad_size) as i32;

                checked_put(center_x + x, center_y + y, color);
                checked_put(center_x + y, center_y + x, color);
            }
        };

        draw_corner(&draw, &mut img, quad_size * 3, true, shape);
    }
}

fn antialias_range(x: f64) -> std::ops::RangeInclusive<u8> {
    let floor = x.floor();
    let ceil = x.ceil();

    floor as u8..=ceil as u8
}

struct Context<'qr> {
    base: (i32, i32),
    is_on: bool,
    module: (i32, i32),
    qr: &'qr QrCode,
    quad_size: u32,
    quad: (i32, i32),
}

fn is_corner(context: &Context, connect_corners: bool) -> bool {
    let (x, y) = context.quad;

    let check = if context.is_on != connect_corners {
        vec![(0, y), (x, 0)]
    } else {
        vec![(x, y), (0, y), (x, 0)]
    };

    check.iter().all(|(x_offset, y_offset)| {
        context
            .qr
            .get_module(context.module.0 + x_offset, context.module.1 + y_offset)
            == !context.is_on
    })
}

fn draw_corner(
    draw: impl Fn(&mut RgbaImage, u32, u32, u8),
    mut img: &mut RgbaImage,
    quad_size: u32,
    is_on: bool,
    shape: Shape,
) {
    for x in 0..=quad_size as u32 {
        let real_y = shape.get_y(x.into(), quad_size);

        for y in antialias_range(real_y) {
            let percent = 1. - (real_y - y as f64).abs();

            if percent < 0. || percent > 1. || y as u32 >= quad_size {
                continue;
            }

            let alpha = if y as f64 > real_y {
                (255. * percent).round() as u8
            } else {
                255
            };

            let alpha = if is_on { alpha } else { 255 - alpha };
            draw(&mut img, x, y.into(), alpha);
        }

        if is_on {
            for y in x..real_y.round() as u32 {
                draw(&mut img, x, y, 255);
            }
        } else {
            for y in (real_y.round() + 1.) as u32..quad_size as u32 {
                draw(&mut img, x, y, 255);
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn lcm_test() {
        assert_eq!(lcm(2, 3), 6);
        assert_eq!(lcm(7, 21), 21);
        assert_eq!(lcm(15, 20), 60);
    }
}
