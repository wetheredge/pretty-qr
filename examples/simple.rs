use pretty_qr::{PrettyQr, Shape};

fn main() {
    PrettyQr::new(500)
        .shape(Shape::Circle)
        .connect_corners(true)
        .render(b"Hello, world!")
        .save("qr.png")
        .expect("failed to write to file");
}
